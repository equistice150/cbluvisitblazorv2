﻿namespace cBluVisitBlazor.Shared
{
    public enum FuelLevel
    {
        Empty,
        Quarter,
        Half,
        ThreeQuarters,
        Full
    }
}
